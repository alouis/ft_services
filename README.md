# Ft_services

## Make program work

Install [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) and [Docker](https://hub.docker.com/search?offering=community&type=edition) (if at 42school, install Minikube in the goinfre and Docker in Management Software Center then use [42ToolBox](https://github.com/alexandregv/42toolbox) // if on your vm check [these steps](https://docs.docker.com/engine/install/linux-postinstall/) after installing Docker)

```sh
brew install kubectl
brew install minikube
export MINIKUBE_HOME=/goinfre/$USER/
rm -rf /Users/login/.minikube
 
chmod +x setup.sh	#when creating a .sh file permission to execute exec must be granted once otherwise use bash setup.sh
./setup.sh
```

## Useful commands :

```sh
# Docker daemon runs Dockerfile's instructions in srcs/servicedir/ and outputs ID of new images, -t tags repo in which to save new image if build succeeds
docker build -t <service> <srcs/servicedir>
docker run -it -p PORT:PORT <service>
docker image prune -a #(remove unused images and save space)

# Check active SSH connections
ps auxwww | grep sshd:

# Test SSH
ssh admin@$(minikube ip) -p 4000

# Get a shell in a pod
kubectl exec -it <pod name> -- sh

# Crash Container
kubectl exec -it $(kubectl get pods | grep <image> | cut -d" " -f1) -- /bin/sh -c "kill 1"

# Export/Import Files from containers
kubectl cp srcs/grafana/grafana.db default/$(kubectl get pods | grep grafana | cut -d" " -f1):/var/lib/grafana/grafana.db

# Check load balancer MetalLB configuration
kubectl describe configmap config -n metallb-system
```

* [Openrc commands](https://medium.com/faun/metallb-configuration-in-minikube-to-enable-kubernetes-service-of-type-loadbalancer-9559739787df)
* [Git pull without merging, how to override local files](https://forum.freecodecamp.org/t/git-pull-how-to-override-local-files-with-git-pull/13216)


### ERRORS

* docker: Error response from daemon: [...] Error starting userland proxy: listen tcp 0.0.0.0:80: bind: address already in use.
OR Requests timeout on nginx server: check if nginx is listening on port 80
```sh
sudo netstat -nl -p tcp  | head
sudo kill PID
docker run -p 80:80 <service>

sudo netstat -plant | grep 80
```

* ftps : # 500 OOPS: could not bind listening IPv4 socket
```sh
# Check active connections
lsof -i | grep ftp
netstat -tpan | grep :21;
ps -ef | grep vsftpd
kill <pid>
```

## Ressources

### Kubernetes
 * [Kubernetes vs. Docker](https://containerjournal.com/topics/container-ecosystems/kubernetes-vs-docker-a-primer/)
 * [kubectl Cheat sheet](https://kubernetes.io/docs/reference/kubectl/overview/)
 * [kubectl Commands](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-strong-getting-started-strong-)

### Minikube
 * [Pushing Docker images](https://minikube.sigs.k8s.io/docs/handbook/pushing/)

### Docker
 * [How to use Docker images with Minikube](https://medium.com/bb-tutorials-and-thoughts/how-to-use-own-local-doker-images-with-minikube-2c1ed0b0968)
 * [Docker documentation](https://docs.docker.com/engine/reference/builder/) & [Best practices for writing Dockerfile](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/) ([In case you need a .dockerignore file](https://docs.docker.com/engine/reference/builder/#dockerignore-file))
 
### Nginx
 * [Nginx config file (FR)](https://doc.ubuntu-fr.org/nginx)
 * [Running nginx in the foreground](https://docs.docker.com/engine/reference/run/)
 * [Proxy pass](http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_pass)

### SSH
 * [SSH config (FR)](https://www.it-connect.fr/chapitres/openssh-configuration-du-serveur-ssh/)
 * [SSH config part2](https://stackoverflow.com/questions/35690954/running-openssh-in-an-alpine-docker-container)

### Alpine 
The --no-cache option allows to not cache the index locally, which is useful for keeping containers small.
Literally it equals apk update in the beginning and rm -rf /var/cache/apk/\* in the end.
* [Alpine documentation](https://wiki.alpinelinux.org/wiki/Main_Page)

### MySQL

* [mysql_install_db](https://mariadb.com/kb/en/mysql_install_db/)
* [Config files](https://mariadb.com/kb/en/configuring-mariadb-with-option-files/#option-groups)
* [mysqld: the MySQL Server](https://dev.mysql.com/doc/refman/5.6/en/mysqld-server.html)
* [Manage MariaDB databes and users](https://phoenixnap.com/kb/how-to-create-mariadb-user-grant-privileges)
* [Dump](https://dev.mysql.com/doc/refman/5.7/en/mysqldump-sql-format.html) & [reload SQL backups](https://dev.mysql.com/doc/refman/5.7/en/reloading-sql-format-dumps.html)

### Wordpress

* [Edit wp-config file](https://wordpress.org/support/article/editing-wp-config-php/)

### PhpMyAdmin
* [Generate](https://docs.phpmyadmin.net/en/latest/setup.html) & [configure config.inc.php](https://docs.phpmyadmin.net/en/latest/config.html#cfg_Servers_control_*)
* [Use SSL for connection to db](https://docs.phpmyadmin.net/en/latest/setup.html)
* [Some proxy_pass examples](https://www.liaohuqiu.net/posts/nginx-proxy-pass/)

### FTPS
* [Un peu de culture](https://www.culture-informatique.net/cest-quoi-un-serveur-ftp/)
* [Some interesting reading](https://wiki.filezilla-project.org/Network_Configuration)
* [How to use FileZilla client](https://wiki.filezilla-project.org/Using)
* [Man 5 vsftpsd.conf](https://security.appspot.com/vsftpd/vsftpd_conf.html)
* [Understanding passive FTP](https://slacksite.com/other/ftp.html)
* [With a little help](https://github.com/onjin/docker-alpine-vsftpd)

### InfluxDB, Telegraf, Grafana
* [Tutorial](https://thenewstack.io/how-to-setup-influxdb-telegraf-and-grafana-on-docker-part-1/)
* [InfluxDB documentation](https://docs.influxdata.com/influxdb/v1.8/introduction/install/)
* [InfluxDB & Telegraf for Kubernetes](https://www.influxdata.com/solutions/kubernetes-monitoring-solution/)
* [Might be a solution](https://octoperf.com/blog/2019/09/19/kraken-kubernetes-influxdb-grafana-telegraf/#map-a-configuration-file-using-configmap)
